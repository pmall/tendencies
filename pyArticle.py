import os
import pandas as pd
import numpy as np
import re
import collections
import matplotlib as mpl
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import seaborn as sns
import sqlite3
import statsmodels.formula.api as smf
import pymannkendall
from statsmodels.tsa.seasonal import STL
from py4pm.chemutilities import pretty_specie, specie_unit, compute_PMreconstructed

mpl.rcParams["figure.dpi"] = 90
mpl.rcParams["axes.grid"] = True
mpl.rcParams["axes.axisbelow"] = True
mpl.rcParams["axes.labelsize"] = "large"

LOCAL_FILTER_PATH = "./BdD_Favez_et_al_2021.xlsx"
LOCAL_METAL_PATH = "./BdD_metals_Favez_et_al_2021.xlsx"

DB_PATH = "/home/webersa/Documents/BdD/bdd_aerosols/aerosols.db"
METALS_PATH = (
    "/home/webersa/Documents/BdD/bdd_aerosols/Metaux_frenes_traitée_un_peu.csv"
)

DATE_MIN = pd.to_datetime("2008-01-01")
DATE_MAX = pd.to_datetime("2020-01-01")

DATE_MIN_8 = pd.to_datetime("2008-01-01")
DATE_MIN_9 = pd.to_datetime("2009-01-01")
DATE_MIN_10 = pd.to_datetime("2010-01-01")
DATE_MIN_11 = pd.to_datetime("2011-01-01")
DATE_MIN_12 = pd.to_datetime("2012-01-01")
DATE_MIN_13 = pd.to_datetime("2013-01-01")
DATE_MIN_14 = pd.to_datetime("2014-01-01")
DATE_MIN_15 = pd.to_datetime("2015-01-01")
DATE_MAX = pd.to_datetime("2020-01-01")


DATE_MIN_AX = pd.to_datetime(DATE_MIN) - pd.DateOffset(months=3)
DATE_MAX_AX = pd.to_datetime(DATE_MAX) + pd.DateOffset(months=3)


ETM = [
    "As",
    "Ba",
    "Cd",
    "Co",
    "Cr",
    "Cu",
    "Mn",
    "Ni",
    "Pb",
    "Sb",
    "Th",
    "V",
    "Zn",
]
IONS = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+", "MSA"]
MONOSACH = ["Levoglucosan", "Mannosan"]
POLYOLS = ["Arabitol", "Mannitol"]
PAH = [
    "Benzo(b)fluorant.",
    "Benzo(k)fluorant.",
    "In.(1,2,3-cd)py.",
    "Benzo(a)anthra.",
    "Dibenzo(a,h)ant.",
    "Benzo(a)pyrene",
    "Benzo(j)fluorant.",
]
AETALOMETER = ["eBC", "eBCff", "eBCwb"]

SPECIES = ["PM10", "OC", "EC"] + IONS + MONOSACH + POLYOLS + ETM + PAH + AETALOMETER

NUMERIC_DATA = (
    ["PM10", "OC", "EC"] + IONS + MONOSACH + POLYOLS + ETM + PAH + AETALOMETER
)

FILTER_DATE_SPECIE = {
    "PM10": [DATE_MIN_8, DATE_MAX],
    "OC": [DATE_MIN_8, DATE_MAX],
    "EC": [DATE_MIN_8, DATE_MAX],
    "EC/OC": [DATE_MIN_8, DATE_MAX],
    "EC/PM10": [DATE_MIN_8, DATE_MAX],
    "TC/PM10": [DATE_MIN_8, DATE_MAX],
    "TC": [DATE_MIN_8, DATE_MAX],
    "MSA": [DATE_MIN_14, DATE_MAX],
    "Cl": [DATE_MIN_9, DATE_MAX],
    "NO3-": [DATE_MIN_8, DATE_MAX],
    "SO42-": [DATE_MIN_8, DATE_MAX],
    "NH4+": [DATE_MIN_8, DATE_MAX],
    "K+": [DATE_MIN_8, DATE_MAX],
    "Na+": [DATE_MIN_11, DATE_MAX],
    "Mg2+": [DATE_MIN_8, DATE_MAX],
    "Ca2+": [DATE_MIN_8, DATE_MAX],
    "Arabitol": [DATE_MIN_12, DATE_MAX],
    "Sorbitol": [DATE_MIN_15, DATE_MAX],
    "Mannitol": [DATE_MIN_12, DATE_MAX],
    "Polyols": [DATE_MIN_12, DATE_MAX],
    "Levoglucosan": [DATE_MIN_11, DATE_MAX],
    "Levoglucosan/OC": [DATE_MIN_11, DATE_MAX],
    "Mannosan": [DATE_MIN_11, DATE_MAX],
    "Al": [DATE_MIN, DATE_MAX],
    "As": [DATE_MIN, DATE_MAX],
    "Ba": [DATE_MIN, DATE_MAX],
    "Cd": [DATE_MIN, DATE_MAX],
    "Co": [DATE_MIN, DATE_MAX],
    "Cr": [DATE_MIN, DATE_MAX],
    "Cs": [DATE_MIN, DATE_MAX],
    "Cu": [DATE_MIN, DATE_MAX],
    "Fe": [DATE_MIN, DATE_MAX],
    "La": [DATE_MIN, DATE_MAX],
    "Mn": [DATE_MIN, DATE_MAX],
    "Mo": [DATE_MIN, DATE_MAX],
    "Ni": [DATE_MIN, DATE_MAX],
    "Pb": [DATE_MIN, DATE_MAX],
    "Rb": [DATE_MIN, DATE_MAX],
    "Sb": [DATE_MIN, DATE_MAX],
    "Se": [DATE_MIN, DATE_MAX],
    "Sn": [DATE_MIN, DATE_MAX],
    "Sr": [DATE_MIN, DATE_MAX],
    "Ti": [DATE_MIN, DATE_MAX],
    "V": [DATE_MIN, DATE_MAX],
    "Zn": [DATE_MIN, DATE_MAX],
    "eBCtotal": [DATE_MIN_15, DATE_MAX],
    "eBCff": [DATE_MIN_15, DATE_MAX],
    "eBCwb": [DATE_MIN_15, DATE_MAX],
    "Benzo(a)pyrene": [DATE_MIN_8, DATE_MAX],
    "Benzo(a)pyrene/ΣPAH": [DATE_MIN_8, DATE_MAX],
    "BaP/PM10": [DATE_MIN_8, DATE_MAX],
    "ΣPAH": [DATE_MIN_8, DATE_MAX],
    "eBC": [DATE_MIN_15, DATE_MAX],
    "eBCff": [DATE_MIN_15, DATE_MAX],
    "eBCwb": [DATE_MIN_15, DATE_MAX],
    "eBC/EC": [DATE_MIN_15, DATE_MAX],
}


def _savefig(filename):
    if not os.path.isdir("./figures"):
        os.mkdir("./figures")

    plt.savefig(
        f"./figures/{filename.replace('/','_over_')}",
    )
    plt.close(plt.gcf())


def format_xticks_as_date(ax):
    # ax.set_xlim(
    #     pd.to_datetime(DATE_MIN_AX).to_pydatetime(),
    #     pd.to_datetime(DATE_MAX_AX).to_pydatetime(),
    # )
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_minor_locator(mdates.MonthLocator(bymonth=[7]))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%b\n%Y"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%b"))
    for label in ax.get_xticklabels():
        label.set_horizontalalignment("center")


# =====================================================================================
# ========= GET DATA PART
# =====================================================================================
def get_data(db_path=None, metals_path=None, site="GRE-fr"):
    """Read data from database

    Parameters
    ==========

    db_path : str
        Chemin d'accès au fichier db
    metals_path : str
        Chemin d'accès au fichier métaux
    site : str
        Site à utiliser

    Returns
    =======

    df : pd.DataFrame
        Actual samples
    df_metals : pd.DataFrame
        Actual samples des métaux
    """
    if db_path is None:
        db_path = DB_PATH
    if metals_path is None:
        metals_path = METALS_PATH

    con = sqlite3.connect(db_path)
    df = pd.read_sql(
        "SELECT * from values_all where Station IS '{}';".format(site),
        con=con,
        parse_dates=["Date"],
    )
    df.set_index("Date", inplace=True)
    capitalize_and_strip_columns(df)
    df = df.drop(["PM10",], axis=1,).rename(
        {
            "PM10AASQA": "PM10",
            "Chrysene_hf": "Chrysene",
            "Benzo(a)anthra._hf": "Benzo(a)anthra.",
            "Retene_hf": "Retene",
            "Benzo(e)pyrene_hf": "Benzo(e)pyrene",
            "Benzo(j)fluorant._hf": "Benzo(j)fluorant.",
            "Benzo(b)fluorant._hf": "Benzo(b)fluorant.",
            "Benzo(k)fluorant._hf": "Benzo(k)fluorant.",
            "Benzo(a)pyrene_hf": "Benzo(a)pyrene",
            "Dibenzo(a,h)ant._hf": "Dibenzo(a,h)ant.",
            "Benzo(g,h,i)per._hf": "Benzo(g,h,i)per.",
            "In.(1,2,3-cd)py._hf": "In.(1,2,3-cd)py.",
            "EBCtotal": "eBC",
            "EBCff": "eBCff",
            "EBCwb": "eBCwb",
        },
        axis=1,
    )

    merge_duplicated_columns(df)

    dfql = pd.read_sql(
        "SELECT * FROM QL WHERE Station IS 'GRE-fr';",
        con=sqlite3.connect(DB_PATH),
        parse_dates=["Date"],
    )
    dfql = dfql.loc[dfql["Sample_ID"].str.contains("QL"), :]
    dfql["Year"] = dfql["Sample_ID"].apply(extract_year_from_str)
    dfql.drop("Sample_ID", axis=1, inplace=True)

    df = replace_leqQL_by_halfQL(df, dfql)

    df_metals = pd.read_csv(metals_path, parse_dates=["Date start"])
    df_metals.drop(["Tag"], axis=1, inplace=True)
    df_metals.set_index("Date start", inplace=True)

    df = df.loc[(df.index > DATE_MIN) & (df.index < DATE_MAX)]
    df_metals = df_metals.loc[df_metals.index < DATE_MAX]
    # Merge tag duplicated row
    df_metals = df_metals.reset_index().groupby("Date start").first()

    for x in df_metals.columns:
        if x in df.columns:
            df.drop(x, axis=1, inplace=True)

    df = pd.merge(df, df_metals, left_index=True, right_index=True, how="outer")
    df.index.names = ["Date"]
    df = df.reset_index().groupby("Date").first()

    df = df.reindex(SPECIES, axis=1)
    df = add_new_pseudo_vars(df)
    df.replace({0: np.nan}, inplace=True)

    return (df, df_metals)


def capitalize_and_strip_columns(df):
    """Rename the column with to their capitalized form
    syringic acid → Syringic acid

    Parameters
    ==========

    df : pd.DataFrame

    """
    df.columns = [
        x[:1].upper() + x[1:] if ((type(x) == str) and (x[:1] != "δ")) else x
        for x in df.columns.str.strip().str.replace(" ", "_")
    ]


def add_new_pseudo_vars(data):
    """Add ratio values or sum of species (i.e. Polyols, etc)

    Parameters
    ----------

    data : pd.DataFrame

    Returns
    -------

    data : pd.DataFrame
        Modified dataframe with new columns
    """

    if [x in data.columns for x in ["Arabitol", "Mannitol"]]:
        data["Polyols"] = data["Arabitol"] + data["Mannitol"]

    data["EC/OC"] = data["EC"] / data["OC"]
    data["EC/PM10"] = data["EC"] / data["PM10"]
    data["Levoglucosan/OC"] = data["Levoglucosan"] / data["OC"] / 1000
    data["TC/PM10"] = (data["EC"] + 1.8 * data["OC"]) / data["PM10"]
    data["BaP/PM10"] = data["Benzo(a)pyrene"] / data["PM10"] / 1000

    data["eBC/EC"] = data["eBC"] / data["EC"]

    data["ΣPAH"] = (
        data["Benzo(b)fluorant."]
        + data["Benzo(k)fluorant."]
        + data["In.(1,2,3-cd)py."]
        + data["Benzo(a)anthra."]
        + data["Dibenzo(a,h)ant."]
        + data["Benzo(a)pyrene"]
        + data["Benzo(j)fluorant."]
    )
    data["Benzo(a)pyrene/ΣPAH"] = data["Benzo(a)pyrene"] / data["ΣPAH"]

    return data


def merge_duplicated_columns(df):
    """Merge 2 duplicated columns

    :df: TODO
    :returns: TODO

    """
    duplicated = set(df.columns[df.columns.duplicated()])
    if len(duplicated) == 0:
        return
    for dup in duplicated:
        try:
            dfduplicated = df[dup].copy()
            merged = dfduplicated.iloc[:, 0]
            for i in range(1, len(dfduplicated.columns)):
                merged = merged.combine_first(dfduplicated.iloc[:, i])
            df.drop(dup, axis="columns", inplace=True)
            df[dup] = merged
        except:
            print("Error for column: ", dup)
            raise ValueError


def extract_year_from_str(x):
    try:
        year = re.findall("\d\d\d\d", x)[0]
    except:
        print("No year for QL", x)
        year = np.nan
    return year


def replace_leqQL_by_halfQL(dfval, dfQL):
    """TODO: Docstring for replace_leqQL_by_halfQL.

    :dfval: TODO
    :dfQL: TODO
    :returns: TODO

    """
    # Replace <LD/<QL by the QL/2 of each specie for each year
    species = list(set(dfQL.columns) & set(dfval.columns))
    dfval["Date"] = dfval.index.get_level_values("Date")
    dfval["Year"] = dfval["Date"].apply(lambda x: x.year)
    year_min = dfval["Date"].min().year
    year_max = dfval["Date"].max().year
    for specie in species:
        for year in range(year_min, year_max + 1):
            halfQL = dfQL.loc[dfQL["Year"] == year, specie].mean() / 2
            replaced = dfval.loc[dfval["Year"] == year, specie].replace(
                {
                    "<QL": halfQL,
                    "<LQ": halfQL,
                    "<LD": halfQL,
                    "<DL": halfQL,
                    -2: halfQL,
                    -1: halfQL,
                },
            )
            dfval.loc[dfval["Year"] == year, specie] = replaced

    dfval.drop(["Date", "Year"], axis=1, inplace=True)

    return dfval


def filter_date(df, specie):
    """Filter the measurement period of a chemical element in a dataframe

    Parameters
    ----------

    df : DataFrame
        Measurement values
    specie : str
        Chemical element

    Returns
    -------

    df : DataFrame,
        Samples of chemical element measurements in the period
    """
    datemin, datemax = FILTER_DATE_SPECIE[specie]
    df = df.loc[(datemin < df.index) & (datemax > df.index)]

    return df


def get_data_preloader(path_filter, path_metals):
    """Read data from prehandled excel files, with everything correct on it.
    
    The file should have bee treated with the `get_data` function before!

    Parameters
    ----------

    path_filter: str
        Path to the excel file with all filters values in it
    path_metals: str
        Path to the excel file with all metals values in it

    Returns
    -------
    df, dfm : pd.DataFrame

    """

    df = pd.read_excel(path_filter, parse_dates=["Date"]).set_index("Date")
    dfm = pd.read_excel(path_metals, parse_dates=["Date start", "Dat end"]).set_index("Date")
    return (df, dfm)

# =====================================================================================
# ========= PLOT PART
# =====================================================================================
def plot_when_do_we_have_what(data, savefig=False):
    """

    Parameters
    ----------

    data : pd.DataFrame
    savefig : boolean, default: False
    """

    fig, ax = plt.subplots(figsize=(7, 6))

    obs = collections.OrderedDict(
        {
            "PAH": dict(
                range=FILTER_DATE_SPECIE["Benzo(a)pyrene"],
                color="orange",
                sampling_type="Filter",
                frequency="daily (1/3)",
            ),
            "eBC": dict(
                range=FILTER_DATE_SPECIE["eBCff"],
                color="#333333",
                sampling_type="Aethalometer",
                frequency="daily",
            ),
            "Metals": dict(
                range=FILTER_DATE_SPECIE["Cu"],
                color="salmon",
                sampling_type="Filter",
                frequency="weekly",
            ),
            pretty_specie("Na+"): dict(
                range=FILTER_DATE_SPECIE["Na+"],
                color="lightblue",
                sampling_type="Filter",
                frequency="daily (1/3)",
            ),
            ", ".join([pretty_specie(x) for x in ["SO42-", "NO3-", "NH4+"]]): dict(
                range=FILTER_DATE_SPECIE["NO3-"],
                color="lightblue",
                sampling_type="Filter",
                frequency="daily (1/3)",
            ),
            "Levoglucosan": dict(
                range=FILTER_DATE_SPECIE["Levoglucosan"],
                color="lightblue",
                sampling_type="Filter",
                frequency="daily (1/3)",
            ),
            "OC, EC": dict(
                range=FILTER_DATE_SPECIE["OC"],
                color="lightblue",
                sampling_type="Filter",
                frequency="daily (1/3)",
            ),
            "PM10": dict(
                range=FILTER_DATE_SPECIE["PM10"],
                color="red",
                sampling_type="TEOM",
                frequency="daily",
            ),
        }
    )

    for i, (specie, values) in enumerate(obs.items()):
        dates = values["range"]
        color = values["color"]
        sampling_type = values["sampling_type"]
        frequency = values["frequency"]

        start = mdates.date2num(dates[0])
        end = mdates.date2num(dates[1])
        width = end - start
        rec = plt.Rectangle(
            xy=(start, i - 0.4),
            width=width,
            height=0.8,
            color=color,
            alpha=1,
            zorder=1,
        )
        ax.add_patch(rec)
        ax.annotate(
            specie,
            (end - 100, i+0.15),
            va="center",
            ha="right",
            fontweight="bold",
            fontsize=14,
            color="white",
        )
        ax.annotate(
            "{}, {}".format(sampling_type, frequency),
            (end - 100, i - 0.2),
            va="center",
            ha="right",
            fontweight="bold",
            fontsize=10,
            color="white",
        )

    ax.set_yticks([])
    ax.set_ylim(-0.5, len(obs.keys()) - 0.5)
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y"))

    ax.set_xlim(DATE_MIN_AX, DATE_MAX_AX)
    sns.despine(left=True)
    fig.subplots_adjust(right=0.89, top=0.95)

    if savefig:
        _savefig("sampling_period")

def plot_annual_mass_balance(data, savefig=False):
    """Bilan de masse annuels moyen des PM10

    Le bilan de masse est calculé selon l'équation définie ici :
    https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall/-/issues/3

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    savefig : boolean, False
        Sauvegarder la figure en png.
    """
    df = compute_PMreconstructed(data, takeOnlyPM10=False)

    variables = ["OM", "EC", "NO3-", "nss-SO42-", "NH4+", "seasalt", "dust"]
    df["PM_obs"] = data["PM10"]
    df["n.d."] = df["PM_obs"] - df[variables].sum(axis=1)
    df = df.groupby(pd.Grouper(freq="Y")).mean()
    df = df.reindex(variables + ["n.d."], axis=1)
    df.index = df.index.year
    df = df.loc[df.index != 2007]

    fig, ax = plt.subplots(figsize=(8, 5))

    df.plot(
        kind="bar",
        ax=ax,
        stacked=True,
        color=[
            "green",
            "black",
            "darkblue",
            "darkred",
            "orange",
            "lightblue",
            "salmon",
            "lightgrey",
        ],
    )

    h, l = ax.get_legend_handles_labels()
    l = [pretty_specie(i) for i in l]
    ax.legend().remove()
    fig.legend(h[::-1], l[::-1], loc="center right", frameon=False, labelspacing=1.4)
    ax.set_ylabel("Concentration (µg.m⁻³)")
    ax.set_xlabel("")
    for t in ax.get_xticklabels():
        t.set_rotation(0)

    fig.subplots_adjust(right=0.8)
    # ax.set_ylim(0, 13)

    ax.set_title(f"Annual mean mass balance concentration")

    ax.axvline(2010.5, ls="--")

    if savefig:
        _savefig(f"annual_mass_balance.png")


def plot_timeseries(data, species, subplots=False, savefig=False):
    """Évolutions temporelles des espèces.

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    species : list
        Les espèces à utiliser
    subplots : boolean, False
        Trace un subplot par espèce
    savefig : boolean, False
        Sauvegarder ou non la figure en png.
    """

    df = data.copy()

    plot_kws = dict(rot=0, marker="o", markerfacecolor="white", markersize=3)

    if subplots:
        height = 2 + 2 * len(species)
        nrows = len(species)
    else:
        height = 4
        nrows = 1

    fig, axes = plt.subplots(figsize=(12, height), nrows=nrows, sharex=True)

    colors = iter(list(mpl.colors.TABLEAU_COLORS.values()))

    if nrows > 1:
        for specie, ax in zip(species, axes):
            dfplot = filter_date(df, specie)
            dfplot[specie].dropna().plot(ax=ax, color=next(colors), **plot_kws)
    else:
        for specie in species:
            dfplot = filter_date(df, specie)
            dfplot[specie].dropna().plot(ax=axes, color=next(colors), **plot_kws)

    if subplots:
        for ax, specie in zip(axes, species):
            h, l = ax.get_legend_handles_labels()
            l = [pretty_specie(specie)]
            ax.legend(h, l)
            if specie in ["OC", "EC", "eBC", "eBCff", "eBCwb", "PM10"]:
                ylabels = "Concentration (µg.m⁻³)"
            elif "/" in species[0]:
                ylabels = "ratio [-]"
            else:
                ylabels = "Concentration (ng.m⁻³)"
            ax.set_ylabel(ylabels)
            ax.set_xlabel("")
        fig.subplots_adjust(
            top=0.925, bottom=0.11, left=0.11, right=0.9, hspace=0.2, wspace=0.2
        )
    else:
        ax = axes
        h, l = ax.get_legend_handles_labels()
        l = [pretty_specie(t) for t in species]
        ax.legend(h, l)
        if species[0] in ["OC", "EC", "eBC", "eBCff", "eBCwb", "PM10"]:
            ylabels = "Concentration (µg.m⁻³)"
        elif "/" in species[0]:
            ylabels = "ratio [-]"
        else:
            ylabels = "Concentration (ng.m⁻³)"
        ax.set_ylabel(ylabels)
        ax.set_xlabel("")

    fig.suptitle(
        "Temporal evolution of {species}".format(
            species=", ".join([pretty_specie(s) for s in species]),
        )
    )

    format_xticks_as_date(ax=ax)

    if savefig:
        _savefig(
            "timeserie_{species}{subplot}.png".format(
                species="-".join(species),
                subplot="_subploted" if subplots else "",
            )
        )


def plot_timeseries_ratio(data, sp1, sp2, aggregate=None, savefig=False):
    """Évolutions temporelles du ratio de 2 espèces.

    data : pd.DataFrame
        Les données
    sp1 : str
        Espèce au nominateur
    sp2 : str
        Espèce au dénominateur
    aggregate : str, None
        Aggrège les données en moyennes selon le pas de temps donnée ("M", "Q", etc).
    savefig : boolean
        Sauvegarder ou non la figure en png.
    """

    df = data.copy()

    fig, ax = plt.subplots(
        figsize=(12, 4),
        nrows=1,
        sharex=True,
    )

    ratio = df[sp1] / df[sp2]

    if aggregate:
        ratio = ratio.resample(aggregate).mean()

    ratio.plot(ax=ax, x_compat=True, rot=0)

    h, l = ax.get_legend_handles_labels()
    l = f"{pretty_specie(sp1)}/{pretty_specie(sp2)}"
    ax.legend(h, [f"{sp1}/{sp2}"])

    ax.set_ylabel(f"ratio {sp1}/{sp2}")
    ax.set_xlabel("")
    ax.set_title(
        "Temporal evolution of the ratio {sp1}/{sp2}".format(
            sp1=pretty_specie(sp1),
            sp2=pretty_specie(sp2),
        )
    )

    format_xticks_as_date(ax=ax)

    ax.axhline(0, ls="--", color="lightgrey")

    if savefig:
        _savefig(
            "timeserie_ratio_{sp1}-{sp2}{aggregate}.png".format(
                sp1=sp1,
                sp2=sp2,
                aggregate=f"_{aggregate}" if aggregate else "",
            )
        )
    return ratio


def plot_correlation(data, elems, savefig=False):
    """Matrice de corrélation

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    elems : list
        Listes des espèces à utiliser
    savefig : boolean
        Sauvegarder ou non la figure en png.
    """

    df = data.copy()

    corr = df[elems].corr("spearman")

    mask = np.zeros_like(corr)
    mask[np.triu_indices_from(mask)] = True

    fig = plt.figure(figsize=(len(elems) / 2 + 1, 8))
    sns.heatmap(corr, vmin=-1, vmax=1, cmap="RdBu_r", annot=True, fmt=".2f", mask=mask)
    ax = plt.gca()
    ax.set_title(f"Corrélation temporelle (spearman)\n({DATE_MIN} à {DATE_MAX})")

    fig.subplots_adjust(top=0.925, bottom=0.15, right=1)

    if savefig:
        _savefig(f"correlation_matrix_spearman.png")


def get_STL_tendency(data, specie, robust=True):
    """Get STL deconvolution

    La déconvolution STL utilises les moyennes mensuelles et est faite avec statsmodels.
    L'options `robust`, qui diminue le poids des événements extremes.
    Si des valeurs sont manquantes, un warning est émis et la valeur manquante est estimé
    par back-filling.

    Parameters
    ----------

    data : pd.DataFrame
    specie : str
        columns of the dataframe
    robust : boolean, default: True
        Lower the weight of extrem events

    Returns
    -------

    reg : STL model
        fitted STL model
    linfit : OLS fit
        linear fit of the trend component, excluding the first and last 6 months

    """

    df = data[specie]
    df = filter_date(df, specie)
    df = df.resample("M").mean()

    if df.isnull().any():
        print(f"Warning: filling NaN value for {specie} (#={df.isnull().sum()})")
        df = df.fillna(method="bfill")

    model = STL(df, robust=robust)
    reg = model.fit()

    # remove first and last 6 month for trend
    df_fit = reg.trend.iloc[6:-6].copy().to_frame()
    df_fit["x"] = range(0, len(df_fit))
    linfit = smf.ols("trend~x", data=df_fit).fit()

    return (reg, linfit)


def get_MK_tendency(data, specie):
    """

    Parameters
    ----------

    data : pd.DataFrame
    specie : str

    Returns
    -------

    Seasonal_Mann_Kendall_Test : Namedtuple
    """
    df = data[specie]
    df = filter_date(df, specie)
    df = df.resample("M").mean()

    Seasonal_Mann_Kendall_Test = pymannkendall.seasonal_test(df, period=12)

    return Seasonal_Mann_Kendall_Test


def plot_tendency(
    reg=None, fitted=None, data=None, specie=None, robust=True, savefig=False
):
    """Trace la déconvolution STL des espèces données, avec la tendances par an.

    La déconvolution STL utilises les moyennes mensuelles et est faite avec statsregs.
    L'options `robust`, qui diminue le poids des événements extremes.
    Si des valeurs sont manquantes, un warning est émis et la valeur manquante est estimé
    par back-filling.


    Parameters
    ----------

    reg : STL reg fitted, default None
        If provided, ``fitted`` should also be provided, but not ``data`` nor ``specie``
    fitted : OLS fit, default None
        If provided, ``reg`` should also be provided, but not ``data`` nor ``specie``
    data : pd.DataFrame, default None
        If provided, ``specie`` should also be provided, but not ``reg`` nor ``fitted``
    specie : str, default None
        If provided, ``data`` should also be provided, but not ``reg`` nor ``fitted``
    robust : boolean, default True
        If STL has to be computed, use robust or not
    savefig : boolean
        Sauvegarder les figures en png
    """

    if all([data, specie]):
        reg, fitted = get_STL_tendency(data, specie=specie, robust=robust)
    else:
        specie = reg.observed.name

    a = fitted.params[1]
    b = fitted.params[0]

    fig, axes = plt.subplots(
        figsize=(12, 7), nrows=4, ncols=1, sharex=True, sharey=False
    )

    axes[0].plot(reg.observed, label="Monthly mean (obs)")
    axes[1].plot(reg.trend, label="Tendency")
    axes[2].plot(reg.seasonal, label="Seasonality")
    axes[3].plot(reg.resid, label="Residual")

    xmin, xmax = reg.trend.index[6], reg.trend.index[-7]
    axes[1].plot(
        [xmin, xmax],
        [b, a * len(fitted.model.endog) + b],
        label="Fit tendency",
    )

    for ax in axes:
        ax.legend(loc="upper right")

    if specie != "T":
        for ax in [axes[0], axes[1]]:
            ax.set_ylim(bottom=0)

    axes[0].set_ylabel("Concentration\n({})".format(specie_unit(specie)))
    axes[1].set_ylabel("Tendency\n({})".format(specie_unit(specie)))
    axes[2].set_ylabel("Saisonality")
    axes[3].set_ylabel("Residual")

    axes[1].annotate(
        "y={a:.3f}x + {b:.3f} (r²={r2:.2f}), i.e. {a_an:.3f} {unit} y⁻¹".format(
            a=a, b=b, r2=fitted.rsquared, a_an=a * 12, unit=specie_unit(specie)
        ),
        xy=(0.05, 0.1),
        xycoords="axes fraction",
        fontsize=12,
        backgroundcolor="white",
    )

    fig.subplots_adjust(bottom=0.1, top=0.9)
    fig.suptitle(f"STL unconvolution of {specie}")

    if savefig:
        _savefig(
            "STL_{specie}{robust}.png".format(
                specie=specie, robust="_robust" if robust else ""
            )
        )


def get_STL_MK_result(data, species, robust=True):
    """Get all regression tendencies results

    Parameters
    ----------

    data : pd.DataFrame
    species : list of str
        species to consider
    robust : boolean, default True
        For STL, use robust or not

    Returns
    -------

    df : pd.DataFrame
        Multicolumns dataframe
    """
    df = pd.DataFrame(
        columns=pd.MultiIndex.from_product(
            [
                ["STL", "MK"],
                ["slope", "slope_unc", "slope_percent", "intercept", "p-val"],
            ]
        ),
        index=species,
    )

    for specie in species:
        reg, fitted = get_STL_tendency(data, specie)
        mk_result = get_MK_tendency(data, specie)
        df.loc[specie] = [
            fitted.params[1] * 12,
            fitted.bse[1] * 12,
            fitted.params[1] * 12 / fitted.model.endog.mean(),
            fitted.params[0],
            fitted.pvalues[1],
            mk_result.slope,
            np.nan,
            mk_result.slope / fitted.model.endog.mean(),
            mk_result.intercept,
            mk_result.p,
        ]

    return df


def plot_STL_MK(df, savefig=False):
    """
    Parameters
    ----------

    df : pd.DataFrame
        MultiColumns dataframe with species as index and [STL, MK] * [slope, slope_unc,
        intercept, p-val] as columns
    savefig : boolean, default False

    """

    def _add_species_plot(names, n, species_groups, df):
        if any([x in names for x in df.index]):
            species_groups.append([x for x in names if x in df.index])
            n += 1
        return (n, species_groups)

    n = 0
    species_groups = []
    n, species_groups = _add_species_plot(["PM10"], n, species_groups, df)
    n, species_groups = _add_species_plot(
        ["OC", "EC", "eBC", "eBCff", "eBCwb"], n, species_groups, df
    )
    n, species_groups = _add_species_plot(
        ["NO3-", "SO42-", "NH4+", "Levoglucosan"], n, species_groups, df
    )
    # n, species_groups = _add_species_plot(["NH4+", "Levoglucosan"], n, species_groups, df)
    n, species_groups = _add_species_plot(["Cu", "Zn"], n, species_groups, df)
    n, species_groups = _add_species_plot(
        ["ΣPAH", "Benzo(a)pyrene"], n, species_groups, df
    )

    fig, axes = plt.subplots(
        figsize=(14, 4),
        ncols=n,
        gridspec_kw=dict(width_ratios=[len(x) for x in species_groups]),
    )
    n = 0
    for species, ax in zip(species_groups, axes):
        y = df.loc[species].xs("slope", level=1, axis=1)
        yerr = df.loc[species].xs("slope_unc", level=1, axis=1)
        pval = df.loc[species].xs("p-val", level=1, axis=1)
        y.plot(kind="bar", ax=ax, yerr=yerr)
        # Iterrating over the bars one-by-one
        for i, bar in enumerate(ax.patches):
            STL_or_MK = ["STL", "MK"][0 if i < (len(ax.patches) / 2) else 1]
            specie = species[i % len(species)]
            p = pval.loc[specie, STL_or_MK]
            if p < 0.001:
                p = "***"
            elif p < 0.01:
                p = "**"
            elif p < 0.05:
                p = "*"
            else:
                p = ""

            ax.annotate(
                p,
                (bar.get_x() + bar.get_width() / 2, 0),
                ha="center",
                va="center",
                xytext=(0, 12),
                textcoords="offset points",
                rotation=90,
            )

    handles, labels = axes[-1].get_legend_handles_labels()
    for ax in axes:
        ax.set_xticklabels(
            [pretty_specie(x.get_text()) for x in ax.get_xticklabels()], rotation=0
        )
        ax.grid(b=False, axis="x")
        ax.legend().remove()

    axes[0].set_ylabel("µg m⁻³ y⁻¹")
    axes[2].set_ylabel("ng m⁻³ y⁻¹")

    fig.legend(handles, labels, loc="lower center", ncol=2, frameon=False)
    fig.subplots_adjust(left=0.06, right=0.96, bottom=0.2, top=0.83, wspace=0.4)
    sns.despine(fig)

    fig.suptitle("Tendencies by year")

    if savefig:
        _savefig("tendencies_STL_MK_all")


def plot_STL_MK_percent(df, savefig=False):
    """
    Parameters
    ----------

    df : pd.DataFrame
        MultiColumns dataframe with species as index and [STL, MK] * [slope, slope_unc,
        slope_percent, intercept, p-val] as columns
    savefig : boolean, default False

    """

    fig, ax = plt.subplots(figsize=(14, 4))

    dfplot = df.xs("slope_percent", level=1, axis=1)
    dfplot.plot(kind="bar", ax=ax)

    ax.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1))

    fig.legend(loc="lower center", ncol=2, frameon=False)

    ax.set_xticklabels(
        [pretty_specie(x.get_text()) for x in ax.get_xticklabels()], rotation=0
    )
    ax.grid(b=False, axis="x")
    ax.legend().remove()

    fig.subplots_adjust(left=0.10, right=0.96, bottom=0.2, top=0.86, wspace=0.4)
    sns.despine(fig)

    fig.suptitle("Tendencies by year divided mean concentration over the period")
    ax.set_ylabel("Tendencies by year (%)")

    if savefig:
        _savefig("tendencies_STL_MK_all_percent")


# =====================================================================================
# ========= Main fonction to run everything
# =====================================================================================
def plot_all(data, savefig=False):
    """Plot the required figures

    Parameters
    ==========

    data : pd.DataFrame
        Données, avec les PM10 et PM2.5
    dfblank : pd.DataFrame
        Données des blancs
    savefig : boolean
        Save or not the figures as png

    Returns
    =======

    Nothing
    """
    # ==== Methods
    plot_when_do_we_have_what(data, savefig=savefig)

    # ==== Barplot of annual mass balance (mean)
    plot_annual_mass_balance(data, savefig=savefig)

    # ==== Raw timeseries of species. If it's a list of species, then they are subploted
    # (i.e. one per row)
    species = [
        ["PM10"],
        ["OC", "EC"],
        ["OC", "EC", "eBCff", "eBCwb"],
        ["EC/OC"],
        ["Levoglucosan/OC"],
        ["EC/PM10"],
        ["TC/PM10"],
        ["BaP/PM10"],
        ["eBCff", "eBCwb"],
        ["eBC/EC"],
        ["NO3-", "SO42-", "NH4+"],
        ["OC"],
        ["EC"],
        ["NO3-"],
        ["NH4+"],
        ["SO42-"],
        ["Levoglucosan"],
        ["MSA"],
        ["Polyols"],
        ["MSA", "Polyols"],
        ["Benzo(a)pyrene"],
        ["Benzo(a)pyrene", "ΣPAH"],
        ["Benzo(a)pyrene/ΣPAH"],
        ["Cu"],
        ["Zn"],
        ["ΣPAH"],
    ]
    for sp in species:
        subploted = True if len(sp) > 1 else False
        plot_timeseries(data, species=sp, savefig=savefig, subplots=subploted)

    # ==== Correlation matrix of Trace element
    # plot_correlation(data, ETMS, savefig=savefig)
    # ==== Correlation matrix of lots of stuff
    # plot_correlation(data, IONS + MONOSACH + ETMS, savefig=savefig)

    # # ==== Yearly boxplot (without fliers)
    # to_plot = [["Al", "Fe"], ["As", "Cd"], ["V", "Ni"], ["Ti", "Cu", "Pb"]]
    # for species in to_plot:
    #     plot_yearly_dispersion(data, species=species, savefig=savefig)

    # ==== Tendancies
    to_plot = [
        "PM10",
        "EC",
        "OC",
        "eBCff",
        "eBCwb",
        "NO3-",
        "NH4+",
        "SO42-",
        "Levoglucosan",
        "Cu",
        "Zn",
        "ΣPAH",
        "Benzo(a)pyrene",
    ]
    for specie in to_plot:
        reg, linfit = get_STL_tendency(data, specie=specie, robust=True)
        plot_tendency(reg=reg, fitted=linfit, savefig=savefig)

    df = get_STL_MK_result(data, to_plot)
    plot_STL_MK(df, savefig=True)
    plot_STL_MK_percent(df, savefig=True)
    df.to_excel("figures/STL_MK_results.xlsx")


def main():
    # df, dfm = get_data()
    df, dfm = get_data_preloader(LOCAL_FILTER_PATH, LOCAL_METAL_PATH)

    plt.interactive(False)
    plot_all(data=df, savefig=True)
    plt.interactive(True)

    print("All Done! Check your ./figures folders now.")


if __name__ == "__main__":
    main()
